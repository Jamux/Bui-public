/*! bui - v0.0.1 - 2019-04-05 
Copyright (c): kevin.huang www.vvui.net 
Released under MIT License*/

!function(e,n){"function"==typeof define&&define.amd?define(["$B"],function(t){return n(e,t)}):(e.$B||(e.$B={}),n(e,e.$B))}("undefined"!=typeof window?window:this,function(t,i){function d(t,e){i.extend(this,d);var n=$.extend(e,{onItemCreated:function(t,e){var n=this.children("a").css({width:"100%","max-width":"100%","text-overflow":" ellipsis",overflow:"hidden","padding-right":"0px"});n.attr("title",n.text())}});this.menu=new i.Menu(t,n),this.menu.treeWrap.width("100%")}return d.prototype={destroy:function(){this.menu.destroy(),this.super.destroy.call(this)}},i.NavMenu=d});