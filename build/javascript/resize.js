/*
 * @Author: kevin.huang 
 * @Date: 2018-07-21 20:16:21 
 * @Last Modified by: kevin.huang
 * @Last Modified time: 2019-03-22 22:10:12
 * Copyright (c): kevin.huang Released under MIT License
 */
(function (global, factory) {
    if (typeof define === 'function' && define.amd  && !window["_all_in_"]) {
        define(['$B', 'plugin'], function (_$B) {
            return factory(global, _$B);
        });
    } else {
        if(!global["$B"]){
            global["$B"] = {};
        }
        factory(global, global["$B"]);
    }
}(typeof window !== "undefined" ? window : this, function (window, $B) {
    var defaultOpts = {
        target: undefined, //需要重置大小的目标元素
        zoomScale: false, //是否等比例缩放
        poitStyle: {
            color: '#FF292E',
            "font-size": "8px"
        }, //8个点配置，设置为undefined则不需要4点
        lineStyle: {
            "border-color": "#FF292E",
            "border-size": "1px"
        },
        onDragReady: undefined,
        dragStart: undefined,
        onDrag: undefined,
        dragEnd: undefined
    };

    function Resize(opts) {
        $B.extend(this, Resize); //继承父类
        var $body = $("body");
        this.opts = $.extend({}, defaultOpts, opts);
        this.target = this.opts.target;
        var _this = this;
        var movingTarget,
            mvFlag,
            zoomScaleUpdateSet = {},
            tgsize,
            tgpos;
        function _dragStart(args) {
            var state = args.state;
            movingTarget = state.target;
            mvFlag = movingTarget.data("resizeData");
            tgsize = {
                height: _this.target.height(),
                width: _this.target.width()
            };
            tgpos = _this.target.position();
            zoomScaleUpdateSet = {};
            zoomScaleUpdateSet["line1"] = {
                width: _this.line1.outerWidth(),
                position: _this.line1.position()
            };
            zoomScaleUpdateSet["line2"] = {
                height: _this.line2.outerHeight(),
                position: _this.line2.position()
            };
            zoomScaleUpdateSet["line3"] = {
                width: _this.line3.outerWidth(),
                position: _this.line3.position()
            };
            zoomScaleUpdateSet["line4"] = {
                height: _this.line4.outerHeight(),
                position: _this.line4.position()
            };
            zoomScaleUpdateSet["point0"] = {
                position: _this.poitArr[0].position()
            };
            zoomScaleUpdateSet["point1"] = {
                position: _this.poitArr[1].position()
            };
            zoomScaleUpdateSet["point2"] = {
                position: _this.poitArr[2].position()
            };
            zoomScaleUpdateSet["point3"] = {
                position: _this.poitArr[3].position()
            };
            _this.zoomScaleUpdateSet = zoomScaleUpdateSet;

            if (_this.opts.dragStart) {
                _this.opts.dragStart.call(movingTarget, args);
            }
        }
        function _zoomUpated(state, _index, _type,update) {
            var movData = state._data;
            var leftOffset = movData.leftOffset;
            var topOffset = movData.topOffset;

            var line1Data = zoomScaleUpdateSet["line1"];
            var line2Data = zoomScaleUpdateSet["line2"];
            var line3Data = zoomScaleUpdateSet["line3"];
            var line4Data = zoomScaleUpdateSet["line4"];
            var targetCss = {};
            if (_type) { //4个点
                if (_this.opts.zoomScale) {
                    if (_index === 0 || _index === 2) {
                        topOffset = leftOffset;
                        movData.top = movData.startTop + topOffset;
                    } else {
                        topOffset = -leftOffset;
                        movData.top = movData.startTop + topOffset;
                    }
                }
                var line1Css = {},
                    line2Css = {},
                    line3Css = {},
                    line4Css = {},
                    otherOfs, w;
                if (_index === 0) {
                    line1Css["top"] = line1Data.position.top + topOffset;
                    line1Css["left"] = line1Data.position.left + leftOffset;
                    line1Css["width"] = line1Data.width - leftOffset;
                    _this.line1.css(line1Css);
                    line2Css["top"] = line2Data.position.top + topOffset;
                    line2Css["height"] = line2Data.height - topOffset;
                    _this.line2.css(line2Css);
                    line3Css["left"] = line3Data.position.left + leftOffset;
                    line3Css["width"] = line3Data.width - leftOffset;
                    _this.line3.css(line3Css);
                    line4Css["height"] = line4Data.height - topOffset;
                    line4Css["top"] = line4Data.position.top + topOffset;
                    line4Css["left"] = line4Data.position.left + leftOffset;
                    _this.line4.css(line4Css);
                    _this._updatePoitPosition(0, topOffset, 1);
                    _this._updatePoitPosition(leftOffset, 0, 3);
                    targetCss["width"] = tgsize.width - leftOffset;
                    targetCss["height"] = tgsize.height - topOffset;
                    targetCss["top"] = tgpos.top + topOffset;
                    targetCss["left"] = tgpos.left + leftOffset;
                } else if (_index === 1) {
                    line1Css["top"] = line1Data.position.top + topOffset;
                    line1Css["width"] = line1Data.width + leftOffset;
                    _this.line1.css(line1Css);
                    line2Css["top"] = line2Data.position.top + topOffset;
                    line2Css["height"] = line2Data.height - topOffset;
                    line2Css["left"] = line2Data.position.left + leftOffset;
                    _this.line2.css(line2Css);
                    line3Css["width"] = line3Data.width + leftOffset;
                    _this.line3.css(line3Css);

                    line4Css["top"] = line4Data.position.top + topOffset;
                    line4Css["height"] = line4Data.height - topOffset;
                    _this.line4.css(line4Css);
                    _this._updatePoitPosition(0, topOffset, 0);
                    _this._updatePoitPosition(leftOffset, 0, 2);
                    targetCss["width"] = tgsize.width + leftOffset;
                    targetCss["height"] = tgsize.height - topOffset;
                    targetCss["top"] = tgpos.top + topOffset;
                } else if (_index === 2) {
                    line1Css["width"] = line1Data.width + leftOffset;
                    _this.line1.css(line1Css);
                    line2Css["height"] = line2Data.height + topOffset;
                    line2Css["left"] = line2Data.position.left + leftOffset;
                    _this.line2.css(line2Css);
                    line3Css["width"] = line3Data.width + leftOffset;
                    line3Css["top"] = line3Data.position.top + topOffset;
                    _this.line3.css(line3Css);
                    line4Css["height"] = line4Data.height + topOffset;
                    _this.line4.css(line4Css);
                    _this._updatePoitPosition(leftOffset, 0, 1);
                    _this._updatePoitPosition(0, topOffset, 3);
                    targetCss["width"] = tgsize.width + leftOffset;
                    targetCss["height"] = tgsize.height + topOffset;
                } else {
                    line1Css["left"] = line1Data.position.left + leftOffset;
                    line1Css["width"] = line1Data.width - leftOffset;
                    _this.line1.css(line1Css);

                    line2Css["height"] = line2Data.height + topOffset;
                    _this.line2.css(line2Css);

                    line3Css["top"] = line3Data.position.top + topOffset;
                    line3Css["left"] = line3Data.position.left + leftOffset;
                    line3Css["width"] = line3Data.width - leftOffset;
                    _this.line3.css(line3Css);

                    line4Css["height"] = line4Data.height + topOffset;
                    line4Css["left"] = line4Data.position.left + leftOffset;
                    _this.line4.css(line4Css);

                    _this._updatePoitPosition(leftOffset, 0, 0);
                    _this._updatePoitPosition(0, topOffset, 2);

                    targetCss["width"] = tgsize.width - leftOffset;
                    targetCss["height"] = tgsize.height + topOffset;
                    targetCss["left"] = tgpos.left + leftOffset;

                }
            } else { //4条边
                if (_index === 0) {
                    otherOfs = topOffset / 2;
                    line2Css = {
                        "top": line2Data.position.top + topOffset
                    };
                    line4Css = {
                        "top": line4Data.position.top + topOffset
                    };
                    _this.line2.outerHeight(line2Data.height - topOffset);
                    _this.line4.outerHeight(line4Data.height - topOffset);
                    var point0ShiftLeft = 0;
                    var point1ShiftLeft = 0;
                    targetCss["top"] = tgpos.top + topOffset;
                    targetCss["height"] = tgsize.height - topOffset;
                    if (_this.opts.zoomScale) { //等比例缩放
                        movData.left = line1Data.position.left + otherOfs;
                        w = line1Data.width - topOffset;
                        _this.line1.outerWidth(w);
                        _this.line3.outerWidth(w);
                        _this.line3.css("left", line3Data.position.left + otherOfs);
                        line2Css["left"] = line2Data.position.left - otherOfs;
                        line4Css["left"] = line4Data.position.left + otherOfs;
                        point0ShiftLeft = otherOfs;
                        point1ShiftLeft = -otherOfs;
                        _this._updatePoitPosition(-otherOfs, 0, 2);
                        _this._updatePoitPosition(otherOfs, 0, 3);
                        targetCss["left"] = tgpos.left + otherOfs;
                        targetCss["width"] = tgsize.width - topOffset;
                    }
                    _this._updatePoitPosition(point0ShiftLeft, topOffset, 0);
                    _this._updatePoitPosition(point1ShiftLeft, topOffset, 1);
                    _this.line2.css(line2Css);
                    _this.line4.css(line4Css);
                } else if (_index === 1) {
                    otherOfs = leftOffset / 2;
                    w = line1Data.width + leftOffset;
                    line1Css = {
                        width: w
                    };
                    line2Css = {
                        left: line1Data.position.left + leftOffset
                    };
                    line3Css = {
                        width: w
                    };
                    targetCss["width"] = tgsize.width + leftOffset;
                    var p1TopOffset = topOffset;
                    var p2TopOffset = topOffset;
                    if (_this.opts.zoomScale) { //等比例缩放
                        movData.top = line2Data.position.top - otherOfs;
                        line2Css["height"] = line2Data.height + leftOffset;
                        p1TopOffset = -otherOfs;
                        p2TopOffset = otherOfs;
                        _this._updatePoitPosition(0, p1TopOffset, 0);
                        _this._updatePoitPosition(0, p2TopOffset, 3);
                        _this.line4.css({
                            height: line2Css["height"],
                            top: line4Data.position.top - otherOfs
                        });
                        _this.line1.css({
                            top: line1Data.position.top - otherOfs
                        });
                        _this.line3.css({
                            top: line3Data.position.top + otherOfs
                        });
                        targetCss["height"] = tgsize.height + leftOffset;
                        targetCss["top"] = tgpos.top - otherOfs;
                    }
                    _this._updatePoitPosition(leftOffset, p1TopOffset, 1);
                    _this._updatePoitPosition(leftOffset, p2TopOffset, 2);
                    _this.line1.css(line1Css);
                    _this.line2.css(line2Css);
                    _this.line3.css(line3Css);
                } else if (_index === 2) {
                    otherOfs = topOffset / 2;
                    var p2LeftOffset = 0;
                    var p3LeftOffset = 0;
                    var h = line2Data.height + topOffset;
                    line2Css = {
                        height: h
                    };
                    line4Css = {
                        height: h
                    };
                    line3Css = {
                        top: line3Data.position.top + topOffset
                    };
                    targetCss["height"] = tgsize.height + topOffset;
                    if (_this.opts.zoomScale) { //等比例缩放
                        movData.left = line3Data.position.left - otherOfs;
                        line3Css["width"] = line3Data.width + topOffset;
                        line1Css = {
                            "width": line3Css["width"],
                            left: line1Data.position.left - otherOfs
                        };
                        _this.line1.css(line1Css);
                        line2Css["left"] = line2Data.position.left + otherOfs;
                        line4Css["left"] = line4Data.position.left - otherOfs;
                        p2LeftOffset = otherOfs;
                        p3LeftOffset = -otherOfs;
                        _this._updatePoitPosition(-otherOfs, 0, 0);
                        _this._updatePoitPosition(otherOfs, 0, 1);
                        targetCss["width"] = tgsize.width + topOffset;
                        targetCss["left"] = tgpos.left - otherOfs;
                    }
                    _this.line2.css(line2Css);
                    _this.line4.css(line4Css);
                    _this.line3.css(line3Css);
                    _this._updatePoitPosition(p2LeftOffset, topOffset, 2);
                    _this._updatePoitPosition(p3LeftOffset, topOffset, 3);
                } else {
                    otherOfs = leftOffset / 2;
                    line1Css = {
                            width: line1Data.width - leftOffset,
                            left: line1Data.position.left + leftOffset
                        },
                        line3Css = {
                            width: line1Css.width,
                            left: line1Css.left
                        },
                        line4Css = {
                            left: line4Data.left - leftOffset
                        };
                    targetCss["width"] = tgsize.width - leftOffset;
                    targetCss["left"] = tgpos.left + leftOffset;
                    var point0TopOffset = 0;
                    var point3TopOffset = 0;
                    if (_this.opts.zoomScale) { //等比例缩放
                        point0TopOffset = otherOfs;
                        point3TopOffset = -otherOfs;
                        _this._updatePoitPosition(0, point0TopOffset, 1);
                        _this._updatePoitPosition(0, point3TopOffset, 2);
                        movData.top = line4Data.position.top + otherOfs;
                        line4Css["height"] = line4Data.height - leftOffset;
                        line2Css = {
                            height: line2Data.height - leftOffset,
                            top: movData.top
                        };
                        _this.line2.css(line2Css);
                        line1Css["top"] = line1Data.position.top + otherOfs;
                        line3Css["top"] = line3Data.position.top - otherOfs;
                        targetCss["height"] = tgsize.height - leftOffset;
                        targetCss["top"] = tgpos.top + otherOfs;
                    }
                    _this.line1.css(line1Css);
                    _this.line3.css(line3Css);
                    _this.line4.css(line4Css);
                    _this._updatePoitPosition(leftOffset, point0TopOffset, 0);
                    _this._updatePoitPosition(leftOffset, point3TopOffset, 3);
                }
            }
            if(_this.target){
                if(update){
                    _this.target.css(targetCss);
                }                
            }else{
                console.log("_this.target.css(targetCss); is null");
            }            
        }
        function _onDrag(args) {
            var state = args.state;
            var _idx = mvFlag.index;
            var _type = mvFlag._type;
            var update = true;
            if (_this.opts.onDrag) {
                var ret = _this.opts.onDrag.call(movingTarget, args);
                if(typeof ret !== "undefined"){
                    update = ret;
                }
            }
            _zoomUpated(state, _idx, _type,update);
        }
        function _dragEnd(args) {
            if (_this.opts.dragEnd) {
                _this.opts.dragEnd.call(movingTarget, args);
            }
            movingTarget = undefined;
            mvFlag = undefined;
        }
        var dragOpt = {
            nameSpace: 'dragrezie', //命名空间，一个对象可以绑定多种拖动方式
            which: 1, //鼠标键码，是否左键1,右键3 才能触发拖动，默认左右键均可
            cursor: 'move',
            axis: undefined, // v or h  水平还是垂直方向拖动 ，默认全向
            onStartDrag: _dragStart,
            onDrag: _onDrag,
            onStopDrag: _dragEnd
        };
        this.line1 = $("<div style='cursor:s-resize;height:3px;position:absolute;border-top:1px solid;display:none;z-index:2147483647' _id='k_resize_line_0' class='k_resize_element k_box_size k_resize_line_0'></div>").appendTo($body);
        this.line2 = $("<div style='cursor:w-resize;width:3px;position:absolute;border-right:1px solid;display:none;z-index:2147483647' _id='k_resize_line_1' class='k_resize_element k_box_size k_resize_line_1'></div>").appendTo($body);
        this.line3 = $("<div style='cursor:s-resize;height:3px;position:absolute;border-bottom:1px solid;display:none;z-index:2147483647' _id='k_resize_line_2' class='k_resize_element k_box_size k_resize_line_2'></div>").appendTo($body);
        this.line4 = $("<div style='cursor:w-resize;width:3px;position:absolute;border-left:1px solid;display:none;z-index:2147483647'_id='k_resize_line_3'  class='k_resize_element k_box_size k_resize_line_3'></div>").appendTo($body);
        dragOpt["cursor"] = "s-resize";
        dragOpt["axis"] = "v";
        this.line1.css(this.opts.lineStyle).draggable(dragOpt).data("resizeData", {
            _type: 0,
            index: 0
        });
        dragOpt["cursor"] = "w-resize";
        dragOpt["axis"] = "h";
        this.line2.css(this.opts.lineStyle).draggable(dragOpt).data("resizeData", {
            _type: 0,
            index: 1
        });
        dragOpt["cursor"] = "s-resize";
        dragOpt["axis"] = "v";
        this.line3.css(this.opts.lineStyle).draggable(dragOpt).data("resizeData", {
            _type: 0,
            index: 2
        });
        dragOpt["cursor"] = "w-resize";
        dragOpt["axis"] = "h";
        this.line4.css(this.opts.lineStyle).draggable(dragOpt).data("resizeData", {
            _type: 0,
            index: 3
        });
        this._fixLineStyle();
        this.poitArr = {};
        var i = 0;
        //var clzIcon = this.opts.poitStyle.icon;        
        var cursor;
        dragOpt["axis"] = undefined;
        while (i < 4) {
            if (i === 0) {
                cursor = "se-resize";
            } else if (i === 1) {
                cursor = "ne-resize";
            } else if (i === 2) {
                cursor = "se-resize";
            } else {
                cursor = "ne-resize";
            }
            dragOpt["cursor"] = cursor;
            var piontHtml = "<div style='display:none;position:absolute;z-index:2147483647;cursor:" + cursor + "' class='k_resize_element k_resize_element_point k_box_size k_resize_point_" + i + "' _id='k_resize_point_" + i + "'></div>";
            var tmp = $(piontHtml).appendTo($body);

            tmp.children().css(this.opts.poitStyle);
            tmp.draggable(dragOpt).data("resizeData", {
                _type: 1,
                index: i
            });
            this.poitArr[i] = tmp;
            i = ++i;
        }
        this.jqObj = $body.children(".k_resize_element");
        if (this.target) {
            this.bind(this.target);
        }
    }
    Resize.prototype = {
        _fixLineStyle: function () {
            this.line1.css({
                "border-left": "none",
                "border-right": "none",
                "border-bottom": "none"
            });
            this.line2.css({
                "border-left": "none",
                "border-top": "none",
                "border-bottom": "none"
            });
            this.line3.css({
                "border-left": "none",
                "border-right": "none",
                "border-top": "none"
            });
            this.line4.css({
                "border-top": "none",
                "border-right": "none",
                "border-bottom": "none"
            });
        },
        setStyle: function (pointStyle, lineStyle) {
            this.line1.css(lineStyle);
            this.line2.css(lineStyle);
            this.line3.css(lineStyle);
            this.line4.css(lineStyle);
            this._fixLineStyle();
            var _this = this;
            Object.keys(this.poitArr).forEach(function (key) {
                _this.poitArr[key].css(pointStyle);
            });
            //console.log("-------- setStyle >>>>>> ");
        },
        zoomScale: function (zoom) {
            this.opts.zoomScale = zoom;
        },
        setTarget: function (target) {
            if (!this.target || this.target[0] !== target[0]) {
                this.target = target;
            }
        },
        bind: function (target) {
            this.setTarget(target);
            this.show();
            var ofs = target.offset();
            var size = {
                width: target.outerWidth(),
                height: target.outerHeight()
            };
            this.line1.css({
                top: (ofs.top - 1) + "px",
                left: ofs.left + "px"
            }).outerWidth(size.width);
            this.line2.css({
                top: ofs.top + "px",
                left: (ofs.left + size.width - 2) + "px",
                height: size.height
            });
            this.line3.css({
                top: (ofs.top + size.height - 2) + "px",
                left: ofs.left + "px",
                width: size.width
            });
            this.line4.css({
                top: ofs.top + "px",
                left: ofs.left - 1 + "px"
            }).outerHeight(size.height);
            this._initPoitPosition();
            return this;
        },
        _updatePoitPosition: function (leftOffset, topOffset, updateKey) {
            var point = this.poitArr[updateKey];
            var poitData = this.zoomScaleUpdateSet["point" + updateKey];
            point.css({
                top: poitData.position.top + topOffset,
                left: poitData.position.left + leftOffset
            });
        },
        _initPoitPosition: function () {
            var poitKesy = Object.keys(this.poitArr);
            if (poitKesy.length > 0) {
                var line1Data = {
                    width: this.line1.outerWidth(),
                    position: this.line1.position()
                };
                var line2Data = {
                    height: this.line2.outerHeight(),
                    position: this.line2.position()
                };
                var line3Data = {
                    width: this.line3.outerWidth(),
                    position: this.line3.position()
                };
                //var line4Data = {height:this.line4.outerHeight(),position:this.line4.position()};
                var _this = this;
                poitKesy.forEach(function (idx) {
                    var key = parseInt(idx);
                    var poit = _this.poitArr[key];
                    var w = poit.width() / 2;
                    var h = poit.height() / 2;
                    var _pos;
                    if (key === 0) {
                        _pos = {
                            top: (line1Data.position.top - h) + "px",
                            left: (line1Data.position.left - 3) + "px"
                        };
                    } else if (key === 1) {
                        _pos = {
                            top: (line1Data.position.top - h) + "px",
                            left: (line2Data.position.left - 2) + "px"
                        };
                    } else if (key === 2) {
                        _pos = {
                            top: (line3Data.position.top - 2) + "px",
                            left: (line2Data.position.left - 2) + "px"
                        };
                    } else if (key === 3) {
                        _pos = {
                            top: (line3Data.position.top - 2) + "px",
                            left: (line3Data.position.left - w) + "px"
                        };
                    }
                    poit.css(_pos);
                });
            }
        },
        _drag: function (flag, opt) {  
            var _this = this;          
            if (flag === "line") {
                this.line1.draggable(opt, "dragrezie");
                this.line2.draggable(opt, "dragrezie");
                this.line3.draggable(opt, "dragrezie");
                this.line4.draggable(opt, "dragrezie");
            }else if (flag === "point") {                
                Object.keys(this.poitArr).forEach(function (idx) {
                    var key = parseInt(idx);
                    var poit = _this.poitArr[key];
                    poit.draggable(opt, "dragrezie");
                });
            }else if(flag === "right"){
                this.line2.draggable(opt, "dragrezie");
                this.line3.draggable(opt, "dragrezie");
                this.poitArr[1].draggable(opt, "dragrezie");
                this.poitArr[2].draggable(opt, "dragrezie");
            }else if(flag === "left"){
                this.line1.draggable(opt, "dragrezie");
                this.line4.draggable(opt, "dragrezie");
                this.poitArr[0].draggable(opt, "dragrezie");
                this.poitArr[3].draggable(opt, "dragrezie");
            }else{
                this.line1.draggable(opt, "dragrezie");
                this.line2.draggable(opt, "dragrezie");
                this.line3.draggable(opt, "dragrezie");
                this.line4.draggable(opt, "dragrezie");              
                Object.keys(this.poitArr).forEach(function (idx) {
                    var key = parseInt(idx);
                    var poit = _this.poitArr[key];
                    poit.draggable(opt, "dragrezie");
                });
            }
        },
        /**
         * 禁用拖动
         * flag[line/point/left/right] 不传值则禁用所有
         * line:禁用线
         * point:禁用点
         * left：禁用上边线，左边线，左边点
         * right:禁用下边线，右边线，右边点
         * **/
        disable: function (flag) {
            this._drag(flag, "disable");
        },
        /**
         * 启用拖动 
         * flag[line/point/left/right] 不传值则启用所有
         * line:启用线
         * point:启用点
         * left：启用上边线，左边线，左边点
         * right:启用下边线，右边线，右边点   
         * **/
        enable: function (flag) {
            this._drag(flag, "enable");
        },
        unbind: function () {            
            this.target = undefined;
            this.hide();
            return this;
        },
        show: function (target) {
            if (target) {
                this.bind(target);
            } else {
                var _this = this;
                this.line1.show();
                this.line2.show();
                this.line3.show();
                this.line4.show();
                Object.keys(this.poitArr).forEach(function (key) {
                    _this.poitArr[key].show();
                });
            }
            return this;
        },
        hide: function (target) {
            if (!target || (this.target && target && target[0] === this.target[0])) {
                this.line1.hide();
                this.line2.hide();
                this.line3.hide();
                this.line4.hide();
                var _this = this;
                Object.keys(this.poitArr).forEach(function (key) {
                    _this.poitArr[key].hide();
                });
            }
            return this;
        },
        isHide: function () {
            return this.line1.css("display") === "none";
        },
        isShow: function () {
            return this.line1.css("display") !== "none";
        },
        destroy: function () {
            this.super.destroy.call(this);
        }
    };
    $B["Resize"] = Resize;
    return Resize;
}));